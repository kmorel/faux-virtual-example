cmake_minimum_required(VERSION 3.3)

project(faux-virtual CXX)

find_package(CUDA)

find_package(VTKm REQUIRED
  COMPONENTS CUDA
  )

include_directories(${VTKm_INCLUDE_DIRS})
link_libraries(${VTKm_LIBRARIES})
cuda_add_executable(faux-virtual faux-virtual.cu)

